FROM node:20-alpine

WORKDIR /app

COPY . /app/

RUN npm install

ENV NODE_ENV production

RUN npm run build

EXPOSE 5000

RUN npm install -g serve

RUN find . -maxdepth 1 -mindepth 1 ! -name 'build' -exec rm -rf {} +

CMD ["npx", "serve", "-s", "build", "-l", "80"]
