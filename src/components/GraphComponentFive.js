import React, { useState, useEffect, useRef } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Graph from 'react-graph-vis';
import ModalComponent from './ModalComponent';
import TableModalComponent from './TableModalComponent'; // Новый компонент
import { FaArrowLeft, FaPlus, FaMinus, FaRedo, FaInfo } from "react-icons/fa";
import DevaIcon from '../icons/zodiac/deva.svg';
import BlizneziIcon from '../icons/zodiac/bliznezi.svg';
import FishIcon from '../icons/zodiac/fish.svg';
import KozerogIcon from '../icons/zodiac/kozerog.svg';
import LionIcon from '../icons/zodiac/lion.svg';
import OvenIcon from '../icons/zodiac/oven.svg';
import RackIcon from '../icons/zodiac/rack.svg';
import SkorpionIcon from '../icons/zodiac/skorpion.svg';
import StrelezIcon from '../icons/zodiac/strelez.svg';
import TelezIcon from '../icons/zodiac/telez.svg';
import VesiIcon from '../icons/zodiac/vesi.svg';
import VodoleyIcon from '../icons/zodiac/vodoley.svg';
import BodyGraphImage from '../icons/zodiac/bodygraph.webp';

const zodiacSigns = [
    { id: 1, name: 'Дева', icon: DevaIcon },
    { id: 2, name: 'Близнецы', icon: BlizneziIcon },
    { id: 3, name: 'Рыбы', icon: FishIcon },
    { id: 4, name: 'Козерог', icon: KozerogIcon },
    { id: 5, name: 'Лев', icon: LionIcon },
    { id: 6, name: 'Овен', icon: OvenIcon },
    { id: 7, name: 'Рак', icon: RackIcon },
    { id: 8, name: 'Скорпион', icon: SkorpionIcon },
    { id: 9, name: 'Стрелец', icon: StrelezIcon },
    { id: 10, name: 'Телец', icon: TelezIcon },
    { id: 11, name: 'Весы', icon: VesiIcon },
    { id: 12, name: 'Водолей', icon: VodoleyIcon },
];

function CombinedComponent() {
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [tableModalIsOpen, setTableModalIsOpen] = useState(false);
    const [selectedNode, setSelectedNode] = useState(null);
    const [selectedDescription, setSelectedDescription] = useState("Пожалуйста, подождите. Загружаю информацию из источника...");
    const [graph, setGraph] = useState(null);
    const [network, setNetwork] = useState(null);
    const [loading, setLoading] = useState(true);
    const [selectedSign, setSelectedSign] = useState(null);
    const [visibleStart, setVisibleStart] = useState(0);
    const networkRef = useRef(null);
    let navigate = useNavigate();
    const location = useLocation();

    const visibleCount = 5;

    useEffect(() => {
        const fetchGraphData = async () => {
            try {
                const response = await fetch(`http://185.201.28.47:6677/load-graph?with_position=true`);
                if (!response.ok) {
                    throw new Error('Failed to fetch graph data');
                }
                const data = await response.json();
                setGraph(data);
                setLoading(false);  // Data loaded, set loading to false
            } catch (error) {
                console.error('Error fetching graph data:', error);
                setLoading(false);  // If there's an error, still set loading to false
            }
        };
        fetchGraphData();
    }, [location.search]);

    useEffect(() => {
        if (networkRef.current && graph) {
            setNetwork(networkRef.current);
            networkRef.current.setOptions({
                interaction: {
                    dragNodes: false, // Disable dragging nodes
                    dragView: false,  // Disable dragging view
                    zoomView: false   // Disable zooming
                }
            });

            // Find the first node with an image
            const firstImageNode = graph.nodes.find(node => node.image);
            if (firstImageNode) {
                setSelectedNode(firstImageNode);
                // Move the network to the position of the first node with an image
                networkRef.current.moveTo({
                    position: { x: firstImageNode.x, y: firstImageNode.y },
                    scale: 0.5,
                });
            }
        }
    }, [graph]);

    useEffect(() => {
        if (network) {
            setLoading(false);
        }
    }, [network]);

    const handleZoomIn = () => {
        if (network) {
            let scale = network.getScale();
            network.moveTo({
                scale: scale * 1.2,
                animation: true // Enable smooth animation
            });
        }
    };

    const handleZoomOut = () => {
        if (network) {
            let scale = network.getScale();
            network.moveTo({
                scale: scale * 0.8,
                animation: true // Enable smooth animation
            });
        }
    };

    const handleResetZoom = () => {
        if (network) {
            network.moveTo({
                position: { x: 0, y: 0 },
                scale: 0.5,
                animation: true // Enable smooth animation
            });
        }
    };

    const fetchNodeText = async (node) => {
        let nodeText = '';
        try {
            const response = await fetch(`http://185.201.28.47:6677/generate?text=${node}`);
            if (!response.ok) {
                throw new Error('Failed to fetch node data');
            }
            const data = await response.json();
            nodeText = data.text;
        } catch (error) {
            console.error('Error fetching node data:', error);
        }
        setSelectedDescription(nodeText);
    };
    const handleNodeSelect = (event) => {
        const { nodes } = event;
        if (nodes.length > 0) {
            const nodeId = nodes[0];
            const node = graph.nodes.find(n => n.id === nodeId);
            if (node) {
                console.log("Node selected:", node);
                fetchNodeText(node.label);
                setSelectedNode(node);
                setModalIsOpen(true);
            }
        }
    };

    const handleZodiacSelect = (zodiacId) => {
        setSelectedSign(zodiacId);
        if (graph && network) {
            const node = graph.nodes.find(n => parseInt(n.label) === zodiacId);
            if (node) {
                network.moveTo({
                    position: { x: node.x, y: node.y },
                    scale: 0.45,
                    animation: true // Enable smooth animation
                });
            }
        }
    };

    const handleScrollUp = () => {
        setVisibleStart((prev) => Math.max(prev - 1, 0));
    };

    const handleScrollDown = () => {
        setVisibleStart((prev) => Math.min(prev + 1, zodiacSigns.length - visibleCount));
    };

    const visibleSigns = zodiacSigns.slice(visibleStart, visibleStart + 12);

    const toggleTableModal = () => {
        if (selectedNode) {
            console.log("Opening Table Modal for node:", selectedNode);
            setTableModalIsOpen(true);
        } else {
            console.log("No node selected.");
        }
    };

    const closeTableModal = () => {
        setTableModalIsOpen(false);
    };

    const getConnectedNodes = (nodeId) => {
        if (!graph) return [];
        const connectedEdges = graph.edges.filter(edge => edge.from === nodeId || edge.to === nodeId);
        const connectedNodeIds = connectedEdges.map(edge => (edge.from === nodeId ? edge.to : edge.from));
        return graph.nodes.filter(node => connectedNodeIds.includes(node.id));
    };

    return (
        <div id="graph-container" style={styles.outerContainer}>
            <div style={styles.leftContainer}>
                <div style={styles.sidebar}>
                    <div style={styles.innerContainer}>
                        {visibleSigns.map((sign) => (
                            <div
                                key={sign.id}
                                style={{
                                    ...styles.iconContainer,
                                    ...(selectedSign === sign.id ? styles.selected : {})
                                }}
                                onClick={() => handleZodiacSelect(sign.id)}
                            >
                                <img src={sign.icon} alt={sign.name} style={styles.icon} />
                                <span style={styles.text}>{sign.name}</span>
                            </div>
                        ))}
                    </div>
                </div>
                <div style={styles.bodyGraphContainer}>
                    <img src={BodyGraphImage} alt="Body Graph" style={styles.bodyGraphImage} />
                </div>
                <div style={styles.sidebar}>
                    <div style={styles.innerContainer}>
                        {visibleSigns.map((sign) => (
                            <div
                                key={sign.id}
                                style={{
                                    ...styles.iconContainer,
                                    ...(selectedSign === sign.id ? styles.selected : {})
                                }}
                                onClick={() => handleZodiacSelect(sign.id)}
                            >
                                <img src={sign.icon} alt={sign.name} style={styles.icon} />
                                <span style={styles.text}>{sign.name}</span>
                            </div>
                        ))}
                    </div>
                    <button onClick={toggleTableModal} style={styles.toggleButton}>
                        Table View
                    </button>
                </div>
            </div>
            <div style={styles.rightContainer}>
                {loading && (
                    <div style={styles.loadingOverlay}>
                        <div>Loading...</div>
                    </div>
                )}
                <div style={styles.controls}>
                    <button onClick={() => window.location.href = 'http://185.201.28.47:8005/'} style={styles.controlButton}>
                        <FaArrowLeft />
                    </button>
                    <button onClick={handleZoomIn} style={styles.controlButton}>
                        <FaPlus />
                    </button>
                    <button onClick={handleZoomOut} style={styles.controlButton}>
                        <FaMinus />
                    </button>
                    <button onClick={handleResetZoom} style={styles.controlButton}>
                        <FaRedo />
                    </button>
                    <button style={styles.controlButton}>
                        <FaInfo />
                    </button>
                </div>

                {graph && !tableModalIsOpen && (
                    <Graph
                        graph={{
                            nodes: graph.nodes.map(node => ({
                                ...node,
                                shape: node.image ? 'circularImage' : 'dot',
                                size: node.image ? 50 : 10,
                                image: node.image ? { selected: node.image, unselected: node.image } : undefined,
                                fixed: node.image ? { x: true, y: true } : false // Fix nodes with images
                            })),
                            edges: graph.edges.map(edge => ({
                                ...edge,
                                color: 'rgba(0,0,0,0)', // Set edge color to fully transparent
                                arrows: {
                                    from: { enabled: true, type: 'arrow', opacity: 0 }, // Set arrow opacity to 0
                                    to: { enabled: false } // Disable the arrow pointing to the node
                                }
                            }))
                        }}
                        options={{
                            layout: {
                                hierarchical: false,
                                improvedLayout: false  // Ensure auto layout is not used
                            },
                            interaction: {
                                dragNodes: false, // Disable dragging nodes
                                dragView: false,  // Disable dragging view
                                zoomView: false   // Disable zooming
                            },
                            edges: {
                                color: 'rgba(0,0,0,0)', // Set edge color to fully transparent
                                smooth: {
                                    type: 'continuous'
                                }
                            },
                            nodes: {
                                shape: 'dot',
                                size: 10
                            },
                            physics: {
                                stabilization: false,
                                barnesHut: {
                                    gravitationalConstant: -30000, // Increase distance between nodes
                                    centralGravity: 0.3,
                                    springLength: 200, // Increase spring length to increase distance between nodes
                                    springConstant: 0.04,
                                    damping: 0.09,
                                    avoidOverlap: 0
                                }
                            },
                            height: '100%',
                            width: '100%'
                        }}
                        getNetwork={(network) => {
                            networkRef.current = network;
                        }}
                        events={{ select: handleNodeSelect }}
                    />
                )}
                {selectedNode && (
                    <ModalComponent
                        isOpen={modalIsOpen}
                        onRequestClose={() => {
                            setModalIsOpen(false);
                            setSelectedNode(null);
                            setSelectedDescription("Пожалуйста, подождите. Загружаю информацию из источника...");
                        }}
                        nodeData={selectedNode}
                        selectedDescription={selectedDescription}
                    />
                )}
                {tableModalIsOpen && (
                    <TableModalComponent
                        isOpen={tableModalIsOpen}
                        onRequestClose={closeTableModal}
                        connectedNodes={getConnectedNodes(selectedNode.id)}
                    />
                )}
            </div>
        </div>
    );
}

const styles = {
    outerContainer: {
        display: 'flex',
        height: '100vh',
    },
    leftContainer: {
        display: 'flex',
        flexDirection: 'row',
        width: '50%',
        backgroundColor: '#333',
    },
    sidebar: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '20%', // Adjusted to take up 20% of the left container
        backgroundColor: '#333',
        padding: '10px',
        overflowY: 'auto', // Make the sidebar scrollable
        maxHeight: '100%', // Ensure the sidebar can scroll fully
    },
    innerContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: '20px',
    },
    iconContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        cursor: 'pointer',
        color: 'white',
        padding: '10px',
        borderRadius: '10px',
        transition: 'background-color 0.3s ease',
    },
    selected: {
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
    },
    icon: {
        width: '40px',
        height: '40px',
        marginBottom: '10px',
        filter: 'invert(1)',
    },
    text: {
        color: 'white',
        textAlign: 'center',
    },
    bodyGraphContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
        padding: '20px',
        width: '60%', // Adjusted to take up the remaining 60% of the left container
    },
    bodyGraphImage: {
        width: '100%',
        height: '100%',
        objectFit: 'cover', // Stretch to fit height
    },
    rightContainer: {
        display: 'flex',
        flexDirection: 'column',
        width: '50%',
        position: 'relative',
    },
    graphContainer: {
        flexGrow: 1,
        position: 'relative',
    },
    tableModalContainer: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        width: '50%',
        backgroundColor: '#333',
        zIndex: 1000,
    },
    table: {
        width: '100%',
        borderCollapse: 'collapse',
        color: 'white',
    },
    tableHeader: {
        backgroundColor: '#444',
        color: '#fff',
        padding: '10px',
        textAlign: 'left',
    },
    tableRow: {
        borderBottom: '1px solid #555',
    },
    tableCell: {
        padding: '10px',
    },
    colorCircle: {
        width: '20px',
        height: '20px',
        borderRadius: '50%',
    },
    controls: {
        position: 'absolute',
        top: '5%',
        left: '5%',
        zIndex: 999,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    controlButton: {
        background: 'none',
        border: '2px solid white',
        cursor: 'pointer',
        fontSize: '24px',
        color: 'white',
        borderRadius: '50%',
        width: '40px',
        height: '40px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: '10px',
    },
    loadingOverlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.7)',
        color: 'white',
        fontSize: '24px',
        zIndex: 1000,
    },
};

export default CombinedComponent;
