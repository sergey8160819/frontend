import React from 'react';
import Modal from 'react-modal';

Modal.setAppElement('#root'); // Установите корневой элемент вашего приложения
function ModalComponent({ isOpen, onRequestClose, nodeData, selectedDescription }) {
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="Node Information"
            style={{
                overlay: {
                    backgroundColor: 'rgba(10, 25, 47, 0.85)' // Полупрозрачный фон вокруг модального окна
                },
                content: {
                    color: 'white', // Цвет текста
                    background: '#0a192f', // Темно-синий цвет фона, космическая тема
                    top: '50%',
                    left: '50%',
                    right: 'auto',
                    bottom: 'auto',
                    marginRight: '-50%',
                    transform: 'translate(-50%, -50%)',
                    border: '1px solid #ccd6f6', // Светло-синий цвет границы
                    borderRadius: '10px', // Скругление углов
                    padding: '20px', // Внутренний отступ
                    width: '80%', // Ширина модального окна
                    maxWidth: '600px', // Максимальная ширина модального окна
                    maxHeight: '80vh', // Максимальная высота модального окна (80% высоты экрана)
                    overflowY: 'auto' // Добавление полосы прокрутки при необходимости
                }
            }}
        >
            <h2 style={{textAlign: 'center'}}>Информация о {nodeData ? nodeData.label : 'Unknown'}:</h2>
            <p style={{margin: '20px', fontSize: '16px', lineHeight: '1.6', overflowY: 'auto'}}>
                {selectedDescription}
            </p>
            <button
                onClick={onRequestClose}
                style={{
                    background: '#0077b5', // Цвет кнопки
                    color: 'white', // Цвет текста на кнопке
                    border: 'none',
                    padding: '10px 20px',
                    borderRadius: '5px',
                    cursor: 'pointer',
                    marginTop: '20px',
                    display: 'block', // Центрирование кнопки
                    marginLeft: 'auto',
                    marginRight: 'auto'
                }}
            >
                Закрыть
            </button>
        </Modal>
    );
}

export default ModalComponent;
