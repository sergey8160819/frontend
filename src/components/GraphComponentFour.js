import React, { useState, useEffect, useRef } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Graph from 'react-graph-vis';
import ModalComponent from './ModalComponent';
import { FaArrowLeft, FaPlus, FaMinus, FaRedo, FaInfo } from "react-icons/fa";

function GraphComponentThree() {
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [selectedNode, setSelectedNode] = useState(null);
    const [selectedDescription, setSelectedDescription] = useState("Пожалуйста, подождите. Загружаю информацию из источника...");
    const [graph, setGraph] = useState(null);
    const [loading, setLoading] = useState(true);
    const networkRef = useRef(null);
    let navigate = useNavigate();
    const location = useLocation();

    useEffect(() => {
        const fetchGraphData = async () => {
            try {
                const searchParams = new URLSearchParams(location.search);
                const response = await fetch(`http://185.201.28.47:6677/load-graph?with_position=true`);
                if (!response.ok) {
                    throw new Error('Failed to fetch graph data');
                }
                const data = await response.json();

                // Filter out nodes with images and create a new graph structure
                const nodesWithoutImages = data.nodes.filter(node => !node.image);
                const centralNode = { id: 'centralNode', label: 'Central Node', size: 50 };
                const edges = nodesWithoutImages.map(node => ({ from: 'centralNode', to: node.id }));

                const newGraph = {
                    nodes: [centralNode, ...nodesWithoutImages],
                    edges: edges,
                };
                setGraph(newGraph);

                setLoading(false);  // Data loaded, set loading to false
            } catch (error) {
                console.error('Error fetching graph data:', error);
                setLoading(false);  // If there's an error, still set loading to false
            }
        };
        fetchGraphData();
    }, [location.search]);

    useEffect(() => {
        if (networkRef.current && graph) {
            networkRef.current.once('afterDrawing', () => {
                // Move the network to the position of the central node
                networkRef.current.moveTo({
                    position: { x: 3500, y: 3000 },
                    scale: 0.3,
                });
            });
        }
    }, [graph]);

    const handleZoomIn = () => {
        if (networkRef.current) {
            let scale = networkRef.current.getScale();
            networkRef.current.moveTo({
                scale: scale * 1.2
            });
        }
    };

    const handleZoomOut = () => {
        if (networkRef.current) {
            let scale = networkRef.current.getScale();
            networkRef.current.moveTo({
                scale: scale * 0.8
            });
        }
    };

    const handleResetZoom = () => {
        if (networkRef.current) {
            networkRef.current.moveTo({
                position: { x: 0, y: 0 },
                scale: 0.5,
            });
        }
    };

    const fetchNodeText = async (node) => {
        let nodeText = '';
        try {
            const response = await fetch(`http://185.201.28.47:6677/generate?text=${node}`);
            if (!response.ok) {
                throw new Error('Failed to fetch node data');
            }
            const data = await response.json();
            nodeText = data.text;
        } catch (error) {
            console.error('Error fetching node data:', error);
        }
        setSelectedDescription(nodeText);
    };

    const handleNodeSelect = (event) => {
        const { nodes } = event;
        if (nodes.length > 0) {
            const nodeId = nodes[0];
            const node = graph.nodes.find(n => n.id === nodeId);
            if (node) {
                fetchNodeText(node.label);
                setSelectedNode(node);
                setModalIsOpen(true);
            }
        }
    };

    return (
        <div id="graph-container" style={{ height: '100vh', width: '100vw', position: 'relative' }}>
            {loading && (
                <div style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'rgba(0,0,0,0.7)',
                    color: 'white',
                    fontSize: '24px',
                    zIndex: 1000,
                }}>
                    <div>Loading...</div>
                </div>
            )}
            <div style={{
                position: 'absolute',
                top: '5%',
                left: '5%',
                zIndex: '999',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center'
            }}>
                <button onClick={() => window.location.href=`http://185.201.28.47:8005/`} style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: '10px'
                }}>
                    <FaArrowLeft />
                </button>
                <button onClick={handleZoomIn} style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: '10px'
                }}>
                    <FaPlus />
                </button>
                <button onClick={handleZoomOut} style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: '10px'
                }}>
                    <FaMinus />
                </button>
                <button onClick={handleResetZoom} style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: '10px'
                }}>
                    <FaRedo />
                </button>
                <button style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <FaInfo />
                </button>
            </div>

            {graph && (
                <Graph
                    graph={graph}
                    options={{
                        layout: {
                            hierarchical: false,
                            improvedLayout: false  // Ensure auto layout is not used
                        },
                        edges: {
                            color: 'rgba(0,0,0,0)', // Set edge color to fully transparent
                            smooth: {
                                type: 'continuous'
                            }
                        },
                        nodes: {
                            shape: 'dot',
                            size: 10
                        },
                        physics: {
                            stabilization: false,
                            barnesHut: {
                                gravitationalConstant: -30000, // Increase distance between nodes
                                centralGravity: 0.3,
                                springLength: 200, // Increase spring length to increase distance between nodes
                                springConstant: 0.04,
                                damping: 0.09,
                                avoidOverlap: 0
                            }
                        },
                        height: '100%',
                        width: '100%'
                    }}
                    getNetwork={(network) => {
                        networkRef.current = network;
                    }}
                    events={{ select: handleNodeSelect }}
                />
            )}
            {selectedNode && (
                <ModalComponent
                    isOpen={modalIsOpen}
                    onRequestClose={() => {
                        setModalIsOpen(false);
                        setSelectedNode(null);
                        setSelectedDescription("Пожалуйста, подождите. Загружаю информацию из источника...");
                    }}
                    nodeData={selectedNode}
                    selectedDescription={selectedDescription}
                />
            )}
        </div>
    );
}

export default GraphComponentThree;
