import React from 'react';
import Modal from 'react-modal';

const TableModalComponent = ({ isOpen, onRequestClose, connectedNodes }) => {
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="Table Modal"
            style={modalStyles}
        >
            <div style={styles.tableContainer}>
                <table style={styles.table}>
                    <thead>
                    <tr>
                        <th style={styles.tableHeader}>Color</th>
                        <th style={styles.tableHeader}>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    {connectedNodes.map((node) => (
                        <tr key={node.id} style={styles.tableRow}>
                            <td style={styles.tableCell}>
                                <div style={{ ...styles.colorCircle, backgroundColor: node.color }} />
                            </td>
                            <td style={styles.tableCell}>{node.label}</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        </Modal>
    );
};

const modalStyles = {
    content: {
        top: '0',
        right: '0',
        bottom: '0',
        left: '50%',
        padding: '0',
        backgroundColor: '#333',
        border: 'none',
        borderRadius: '0',
    },
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
    },
};

const styles = {
    tableContainer: {
        padding: '20px',
        backgroundColor: '#333',
        borderRadius: '10px',
        boxShadow: '0 0 10px rgba(0,0,0,0.1)',
        overflow: 'hidden',
        height: '100%',
        width: '100%',
    },
    table: {
        width: '100%',
        borderCollapse: 'collapse',
        color: 'white',
    },
    tableHeader: {
        backgroundColor: '#444',
        color: '#fff',
        padding: '10px',
        textAlign: 'left',
    },
    tableRow: {
        borderBottom: '1px solid #555',
    },
    tableCell: {
        padding: '10px',
    },
    colorCircle: {
        width: '20px',
        height: '20px',
        borderRadius: '50%',
    },
};

export default TableModalComponent;
