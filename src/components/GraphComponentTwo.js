import React, { useState, useEffect, useRef } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Graph from 'react-graph-vis';
import ModalComponent from './ModalComponent';
import { FaArrowLeft, FaPlus, FaMinus, FaRedo, FaInfo } from "react-icons/fa";

function GraphComponent() {
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [selectedNode, setSelectedNode] = useState(null);
    const [selectedDescription, setSelectedDescription] = useState("Пожалуйста, подождите. Загружаю информацию из источника...");
    const [graph, setGraph] = useState(null);
    const [network, setNetwork] = useState(null);
    const networkRef = useRef(null);
    let navigate = useNavigate();
    const location = useLocation();

    useEffect(() => {
        const fetchGraphData = async () => {
            try {
                const searchParams = new URLSearchParams(location.search);
                const response = await fetch(`http://185.201.28.47:6677/load-graph?with_position=true`);
                if (!response.ok) {
                    throw new Error('Failed to fetch graph data');
                }
                const data = await response.json();
                setGraph(data);
            } catch (error) {
                console.error('Error fetching graph data:', error);
            }
        };
        fetchGraphData();
    }, [location.search]);

    useEffect(() => {
        if (networkRef.current) {
            setNetwork(networkRef.current);
        }
    }, [networkRef, graph]);

    const handleZoomIn = () => {
        if (network) {
            let scale = network.getScale();
            network.moveTo({
                scale: scale * 1.2
            });
        }
    };
    const handleResetZoom = () => {
        if (network) {
            network.moveTo({
                position: {x: 0, y: 0}, // Resets the position to center
                scale: 0.15, // Resets zoom to the original scale

            });
        }
    };

    const fetchNodeText = async (node) => {
        let nodeText = '';
        try {
            const response = await fetch(`http://185.201.28.47:6677/generate?text=${node}`);
            if (!response.ok) {
                throw new Error('Failed to fetch graph data');
            }
            const data = await response.json();
            nodeText = data.text;
        } catch (error) {
            console.error('Error fetching graph data:', error);
        }
        setSelectedDescription(nodeText);
    };

    const handleNodeSelect = (event) => {
        const { nodes } = event;
        if (nodes.length > 0) {
            const nodeId = nodes[0];
            const node = graph.nodes.find(n => n.id === nodeId);
            if (node) {
                fetchNodeText(node.label);
                setSelectedNode(node);
                setModalIsOpen(true);
            }
        }
    };
    const handleZoomOut = () => {
        if (network) {
            let scale = network.getScale();
            network.moveTo({
                scale: scale * 0.8
            });
        }
    };

    return (
        <div id="graph-container" style={{height: '100vh', width: '100vw'}}>
            <div style={{
                position: 'absolute',
                top: '5%',
                left: '5%',
                zIndex: '999',
                display: 'flex', // Ensures flexbox layout
                flexDirection: 'row', // Aligns children in a row
                alignItems: 'center' // Vertically centers the buttons
            }}>
                <button onClick={() => window.location.href=`http://185.201.28.47:8005/`} style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: '10px' // Adds space between the buttons
                }}>
                    <FaArrowLeft/>
                </button>
                <button onClick={handleZoomIn} style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: '10px' // Adds space between the buttons
                }}>
                    <FaPlus/>
                </button>
                <button onClick={handleZoomOut} style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: '10px'
                }}>
                    <FaMinus/>
                </button>
                <button onClick={handleResetZoom} style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: '10px' // Adds space between the buttons
                }}>
                    <FaRedo/>
                </button>
                <button style={{
                    background: 'none',
                    border: '2px solid white',
                    cursor: 'pointer',
                    fontSize: '24px',
                    color: 'white',
                    borderRadius: '50%',
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <FaInfo />

                </button>

            </div>

            {graph && (
                <Graph

                    graph={{
                        nodes: graph.nodes.map(node => ({
                            ...node,
                            shape: node.image ? 'circularImage' : 'dot',
                            size: node.image ? 50: 10,
                            image: node.image ? { selected: node.image, unselected: node.image } : undefined
                        })),
                        edges: graph.edges
                    }}
                    options={{
                        layout: {
                            hierarchical: false,
                            improvedLayout: false  // Убедитесь, что не используется автоматичес
                        },
                        edges: {
                            color: 'white',
                            smooth: {
                                type: 'continuous'
                            }
                        },
                        nodes: {
                            shape: 'dot',

                            size: 10
                        },
                        physics: {
                            stabilization: false,
                            barnesHut: {
                                gravitationalConstant: -3000,
                                centralGravity: 0.3,
                                springLength: 95,
                                springConstant: 0.04,
                                damping: 0.09,
                                avoidOverlap: 0
                            }
                        },
                        height: '100%',
                        width: '100%'
                    }}

                    getNetwork={(network) => {
                        networkRef.current = network;
                    }}
                    events={{select: handleNodeSelect}}
                />
            )}
            {selectedNode && (
                <ModalComponent
                    isOpen={modalIsOpen}
                    onRequestClose={() => {
                        setModalIsOpen(false);
                        setSelectedNode(null);
                        setSelectedDescription("Пожалуйста, подождите. Загружаю информацию из источника...");
                    }}
                    nodeData={selectedNode}
                    selectedDescription={selectedDescription}
                />
            )}
        </div>
    );
}

export default GraphComponent;
