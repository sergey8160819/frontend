import React, { useCallback, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { useNavigate } from "react-router-dom";

export default function HomePage() {
    const [fileLinks, setFileLinks] = useState(null);  // Файл со связями
    const [fileLabels, setFileLabels] = useState(null);  // Файл с подписями для корневых точек
    const [menu, setMenu] = useState(null);  // Файл с подписями для корневых точек
    let navigate = useNavigate();

    const onDropLinks = useCallback(acceptedFiles => {
        const file = acceptedFiles[0];
        setFileLinks(file);
    }, []);

    const onDropLabels = useCallback(acceptedFiles => {
        const file = acceptedFiles[0];
        setFileLabels(file);
    }, []);

    const onDropMenu = useCallback(acceptedFiles => {
        const file = acceptedFiles[0];
        setMenu(file);
    }, []);

    const { getRootProps: getRootPropsLinks, getInputProps: getInputPropsLinks, isDragActive: isDragActiveLinks } = useDropzone({
        onDrop: onDropLinks,
        accept: '.xlsx'
    });

    const { getRootProps: getRootPropsLabels, getInputProps: getInputPropsLabels, isDragActive: isDragActiveLabels } = useDropzone({
        onDrop: onDropLabels,
        accept: '.xlsx'
    });

    const { getRootProps: getRootPropsMenu, getInputProps: getInputPropsMenu, isDragActive: isDragActiveMenu } = useDropzone({
        onDrop: onDropMenu,
        accept: '.xlsx'
    });

    const createMap = async () => {
        if (!fileLinks || !fileLabels || !menu) {
            console.error('Both files are required');
            return;
        }

        const formData = new FormData();
        formData.append('file_links', fileLinks);
        formData.append('file_labels', fileLabels);
        formData.append('menu', menu);

        try {
            const response = await fetch('http://185.201.28.47:6677/storage/upload', {
                method: 'POST',
                body: formData
            });

            if (!response.ok) {
                throw new Error('Failed to upload files');
            }
            navigate(`/map`);
        } catch (error) {
            console.error('Error:', error);
        }
    };

    return (
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100vh'
        }}>
            <div style={{
                padding: '20px',
                border: '2px solid #ccc',
                backgroundColor: 'white',
                width: '400px',
                textAlign: 'center',
                marginBottom: '20px'
            }}>
                <a href="http://185.201.28.47:8005/" target="_blank" rel="noopener noreferrer"
                   style={{margin: '10px'}}>Главная</a>
                <a href="https://olegpash.tech/scatter?filename=files/oleg.xlsx" target="_blank"
                   rel="noopener noreferrer"
                   style={{margin: '10px'}}>Scatter</a>
                <a href="https://olegpash.tech/heatmap?filename=files/oleg.xlsx" target="_blank"
                   rel="noopener noreferrer"
                   style={{margin: '10px'}}>Heatmap</a>
                <a href="https://olegpash.tech/heatmap2?filename=files/oleg.xlsx" target="_blank"
                   rel="noopener noreferrer"
                   style={{margin: '10px'}}>Heatmap #2</a>
                <a href="/map" target="_blank" rel="noopener noreferrer"
                   style={{margin: '10px'}}>Map without positions</a>
                <a href="/map2" target="_blank" rel="noopener noreferrer"
                   style={{margin: '10px'}}>Map with positions</a>
                <a href="/map3" target="_blank" rel="noopener noreferrer"
                   style={{margin: '10px'}}>Map with solo circle view</a>
                <a href="/map4" target="_blank" rel="noopener noreferrer"
                   style={{margin: '10px'}}>Map with solo circle view and without arrows</a>
                <a href="/map2" target="_blank" rel="noopener noreferrer"
                   style={{margin: '10px'}}>Map with zodiacs</a>
            </div>
            <div style={{
                padding: '20px',
                border: '2px dashed #ccc',
                backgroundColor: 'white',
                width: '400px',
                textAlign: 'center',
                marginBottom: '20px'
            }}>
                <div {...getRootPropsLinks()}>
                    <input {...getInputPropsLinks()} />
                    {isDragActiveLinks ? <p>Отпустите файл здесь...</p> :
                        <p>Перетащите сюда файл со связями (.xlsx) или кликните для выбора файла</p>}
                </div>
                {fileLinks && <p>Файл загружен: {fileLinks.path}</p>}
            </div>
            <div style={{
                padding: '20px',
                border: '2px dashed #ccc',
                backgroundColor: 'white',
                width: '400px',
                textAlign: 'center',
                marginBottom: '20px'

            }}>
                <div {...getRootPropsLabels()}>
                    <input {...getInputPropsLabels()} />
                    {isDragActiveLabels ? <p>Отпустите файл здесь...</p> :
                        <p>Перетащите сюда файл с подписями для корневых точек (.xlsx) или кликните для выбора
                            файла</p>}
                </div>
                {fileLabels && <p>Файл загружен: {fileLabels.path}</p>}
            </div>

            <div style={{
                padding: '20px',
                border: '2px dashed #ccc',
                backgroundColor: 'white',
                width: '400px',
                textAlign: 'center'
            }}>
                <div {...getRootPropsMenu()}>
                    <input {...getInputPropsMenu()} />
                    {isDragActiveMenu ? <p>Отпустите файл здесь...</p> :
                        <p>Перетащите сюда файл с меню</p>}
                </div>
                {menu && <p>Файл загружен: {menu.path}</p>}
            </div>
            <button onClick={createMap} style={{marginTop: '20px'}}>Создать карту</button>
        </div>
    );
}
