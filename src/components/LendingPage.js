import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import  './MenuTextAnimation.css';


function LandingPage() {
    let navigate = useNavigate();
    const [menuItems, setMenuItems] = useState([]);
    const [selectedText, setSelectedText] = useState('');
    const [menuOpen, setMenuOpen] = useState(false);
    const [isVisible, setIsVisible] = useState(false);
    const [arcData, setArcData] = useState(null);

    useEffect(() => {
        async function fetchArcData() {
            try {
                const response = await fetch('https://cdn.plot.ly/world_110m.json');
                const data = await response.json();
                // Предполагается, что дуги SVG находятся в свойстве arcs
                setArcData(data.arcs);
            } catch (error) {
                console.error('Ошибка загрузки SVG дуг:', error);
            }
        }

        fetchArcData();
    }, []);


    useEffect(() => {
        async function fetchMenuItems() {
            try {
                const response = await fetch('http://185.201.28.47:6677/menu');
                const data = await response.json();
                setMenuItems(data);
            } catch (error) {
                console.error('Failed to load menu items:', error);
            }
        }
        fetchMenuItems();
    }, []);

    const goToMap = () => {
        navigate('/map');
    };

    const toggleMenu = () => {
        setMenuOpen(!menuOpen);
    };

    const handleMenuItemClick = (text) => {
        setIsVisible(false); // Показать текст
        setSelectedText(text);
        setIsVisible(true); // Показать текст
        setMenuOpen(false);
    };

    return (
        <div style={{
            display: 'flex',
            height: '100vh', // Full height of the viewport
            alignItems: 'center', // Vertically center the content
            justifyContent: 'center', // Horizontally center the content
        }}>
            {arcData && (
                <svg width={800} height={600}>
                    {/* Отобразите SVG дуги */}
                    {/* Пример: */}
                    {arcData.map((arc, index) => (
                        <path d="M100,100 A50,50 0 0,1 200,100" fill="none" stroke="black" strokeWidth="2" />
                    ))}
                </svg>
            )}
            <div style={{
                position: 'absolute',
                top: '20px',
                right: '20px',
            }}>
                <div onClick={toggleMenu} style={{
                    width: '30px',
                    height: '25px',
                    cursor: 'pointer',
                }}>
                    <div style={{ height: '4px', background: 'white', margin: '4px 0' }}></div>
                    <div style={{ height: '4px', background: 'white', margin: '4px 0' }}></div>
                    <div style={{ height: '4px', background: 'white', margin: '4px 0' }}></div>
                </div>
                {menuOpen && (
                    <div style={{
                        position: 'absolute',
                        background: 'black',
                        padding: '10px',
                        right: 0,
                        top: '30px',
                    }}>
                        {menuItems.map((item, index) => (
                            <div key={index} onClick={() => handleMenuItemClick(item.text)} style={{
                                color: 'white',
                                cursor: 'pointer',
                                padding: '10px'
                            }}>
                                {item.title}
                            </div>
                        ))}
                    </div>
                )}
            </div>
            <div style={{
                flex: 1,
                padding: '20px',
                marginTop: '20%',
                marginLeft: '5%',
                textAlign: 'center'
            }}>
                <p style={{ fontSize: '36px', fontWeight: 'bold', marginBottom: '10px', color: "white" }}>
                    A little story about space..
                </p>
                <p style={{ fontSize: '18px', fontWeight: 'normal', color: "white", padding: 20 }}>
                    Dive into the fascinating world of space missions, launches, and historical milestones. Explore
                    real-time data visualizations and get insights into the future of space exploration.
                </p>
            </div>

            <div style={{
                flex: 1,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
                textAlign: 'center',
                height: '100%',
                padding: '20px'
            }}>
                <h3 className={`text-animation ${isVisible ? 'visible' : ''}`} style={{
                    color: "white",

                }}>
                    {selectedText}
                </h3>
                <div style={{
                    marginBottom: '20px',
                    textAlign: 'center',
                }}>
                    <button style={{
                        padding: '10px 20px',
                        fontSize: '16px',
                        color: '#007BFF',
                        backgroundColor: 'transparent',
                        border: '2px solid #007BFF',
                        borderRadius: '5px',
                        cursor: 'pointer',
                        marginBottom: '20px',
                        marginTop: '100%' // Добавьте это свойство
                    }} onClick={goToMap}>
                        Start
                    </button>

                </div>
            </div>
        </div>
    );
}

export default LandingPage;
