import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./components/HomePage";
import GraphComponent from "./components/GraphComponent";
import LendingPage from "./components/LendingPage";
import GraphComponentTwo from "./components/GraphComponentTwo";
import GraphComponentThree from "./components/GraphComponentThree";
import GraphComponentFour from "./components/GraphComponentFour";
import GraphComponentFive from "./components/GraphComponentFive";

function App() {
    return (
        <div className="background">
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<LendingPage/>}/>
                    <Route path="/start" element={<HomePage/>}/>
                    <Route path="/map" element={<GraphComponent />} />
                    <Route path="/map2" element={<GraphComponentTwo />} />
                    <Route path="/map3" element={<GraphComponentThree />} />
                    <Route path="/map4" element={<GraphComponentFour />} />
                    <Route path="/map5" element={<GraphComponentFive />} />
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
